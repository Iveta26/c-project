# README

## Filenames
* main.c - used for reading from a file given and for calling the appropriate functions from functions.h file 
* functions.c - has all the implementation of all of the functionality needed to run the program. This includes: storing data to nodes, creating a linked listed using the nodes, printing linked list, calculating distance between two points, time between two events, writing to a .csv file, calculating time of different behaviours and determining what behaviour was actioned
* funtions.h - has all the function declarations so that the functions could be used throughout all source files. Contains two structs 
## How to Compile
### On CLion
1. Navigate to the main.c file
2. Right click within the file
3. Click Run 'untitled1'

### Through Command Line
Compilation is done using the C99 standard.
>Navigate to 'sheepBehaviour' directory wherever you saved it on your PC.  

Based on whether you are using Windows or Linux, do the following to compile and run:
#### On Windows
> gcc main.c functions.c functions.h -o main && ./main
#### On Linux
> g++ main.c functions.c functions.h -o main && ./main

## Libraries Used
* malloc.h - version C99
* string.h - version C99
* malloc.h - version C99
* stdio.h - version C99
* time.h - version C99
* math.h - version C99
* stdlib.h - version C99