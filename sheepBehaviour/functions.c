//
// Created by Iveta on 28/11/2020.
//

#include <malloc.h>
#include "functions.h"
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#define degToRad(angleInDegrees) ((angleInDegrees) * M_PI / 180.0)
#define radToDeg(angleInRadians) ((angleInRadians) * 180.0 / M_PI)



/**
 * creating a node for a linked list
 * @param new_longitude
 * @param new_latitude
 * @param new_day
 * @param new_month
 * @param new_year
 * @param new_hours
 * @param new_min
 * @param new_sec
 * @return
 */
data_ptr make_node(double new_longitude, double new_latitude, int new_day, int new_month, int new_year, int new_hours, int new_min, int new_sec) {
    data_ptr new_node_ptr;
    new_node_ptr = malloc(sizeof(node)); //request area of bytes that the node needs

    if (new_node_ptr == NULL) {
        // we failed to get any memory so we return
        return NULL;
    } else {
        //there's memory to copy in the data
        new_node_ptr -> longitude = new_longitude;
        new_node_ptr -> latitude =  new_latitude;
        new_node_ptr -> day = new_day;
        new_node_ptr -> month = new_month;
        new_node_ptr -> year = new_year;
        new_node_ptr -> hours = new_hours;
        new_node_ptr -> min = new_min;
        new_node_ptr -> sec = new_sec;
        new_node_ptr -> next = NULL;
        return new_node_ptr;
    }
}

//storing nodes to linked list
void insert_at_top_move(movements_ptr * ptr_to_existing_head, movements_ptr new_node_ptr) {
    new_node_ptr->next = *ptr_to_existing_head;
    *ptr_to_existing_head = new_node_ptr;
}

void insert_at_top(data_ptr * ptr_to_existing_head, data_ptr new_node_ptr) {
    new_node_ptr->next = *ptr_to_existing_head;
    *ptr_to_existing_head = new_node_ptr;
}

void display_list(data_ptr list_ptr){
    // printf("The nodes in the list are as follows.\n");
    while (list_ptr != NULL){
        printf("%lf ",list_ptr->longitude);
        printf("%lf ",list_ptr->latitude);
        printf("%d/",list_ptr->day);
        printf("%d/",list_ptr->month);
        printf("%d ",list_ptr->year);
        printf("%d:", list_ptr->hours);
        printf("%d:", list_ptr->min);
        printf("%d\n", list_ptr->sec);
        list_ptr = list_ptr->next;
    }
}

//printing node from list
void display_listMovement(movements_ptr list_ptr){
    // printf("The nodes in the list are as follows.\n");
    while (list_ptr != NULL){
        printf("Start coord: %lf ",list_ptr->currentNode->longitude);
        printf("%lf \n",list_ptr->currentNode->latitude);
        printf("End coord: %lf ",list_ptr->nextNode->longitude);
        printf("%lf/\n",list_ptr->nextNode->latitude);
        printf("Start date: %d/",list_ptr->currentNode->day);
        printf("%d/", list_ptr->currentNode->month);
        printf("%d \n", list_ptr->currentNode->year);
        printf("End date: %d/", list_ptr->nextNode->day);
        printf("%d/ ",list_ptr->nextNode->month);
        printf("%d \n", list_ptr->nextNode->year);
        printf("Start time: %d:", list_ptr->currentNode->hours);
        printf("%d:", list_ptr->currentNode->min);
        printf("%d \n", list_ptr->currentNode->sec);
        printf("End time: %d:", list_ptr->nextNode->hours);
        printf("%d:", list_ptr->nextNode->min);
        printf("%d\n", list_ptr->nextNode->sec);
        printf("Speed: %lf\n", list_ptr->speed);
        printf("Distance: %lf\n", list_ptr->distance);
        printf("Time taken: %d\n", list_ptr->time);
        printf("Behaviour: %s\n", list_ptr->behaviour);
        list_ptr = list_ptr->next;
    }
}


void insert_at_tail(data_ptr * ptr_to_existing_head, data_ptr new_node_ptr){
    // get a temporary pointer to go down the list
    data_ptr * temp_ptr;
    temp_ptr = ptr_to_existing_head;

    // do until pointing at a link containing NULL
    while (*temp_ptr != NULL) {
        temp_ptr = &((*temp_ptr)->next);
    }
    // make new node point where the old link pointed
    // and then make the old last link point to us
    new_node_ptr->next = *temp_ptr;
    *temp_ptr = new_node_ptr;
}

/**
* The great circle distance or the orthodromic distance is the shortest distance
* between two points on a sphere
* @param first - a structure containing (at least) the fields Double lng and Double lat
* @param second - a structure containing (at least) the fields Double lng and Double lat
* @return distance in m
*/
double distance(data_ptr first, data_ptr second)
{
    double lon1 = degToRad(first->longitude);
    double lon2 = degToRad(second->longitude);
    double lat1 = degToRad(first->latitude);
    double lat2 = degToRad(second->latitude);
// Haversine formula (alt. vincenties formula)
    double dlon = lon2 - lon1;
    double dlat = lat2 - lat1;
    double a = pow(sin(dlat / 2), 2)
               + cos(lat1) * cos(lat2)
                 * pow(sin(dlon / 2),2);
    double c = 2 * asin(sqrt(a));
// Radius of earth in kilometers. Use 3956
// for miles
    double r = 6371;
// calculate the result (in m)
    return((c * r)*1000);
}

/**
 * creates a node that stores processed data
 * that will later be stored in a list
 * @param list_ptr
 * @return
 */
movements_ptr createMovementNode(data_ptr list_ptr){
    int timeSeconds;
    double distanceMeters;
    double speedMS;
    char behave[12];
    movements_ptr  new_node_ptr;
    new_node_ptr = malloc(sizeof(move)); //request area of bytes that the node needs

    if (new_node_ptr == NULL) {
        // we failed to get any memory so we return
        return NULL;
    }//else create node
    else{
        data_ptr nextNode = list_ptr->next;
        new_node_ptr ->currentNode = list_ptr;
        new_node_ptr ->nextNode = list_ptr ->next;
        timeSeconds = timeTaken(list_ptr, nextNode);
        distanceMeters = distance(list_ptr, nextNode);
        speedMS = distanceMeters/timeSeconds;
        new_node_ptr -> time = timeSeconds;
        new_node_ptr -> distance = distanceMeters;
        new_node_ptr -> speed = speedMS;

        //determining behaviour by speed
        if(0 <= speedMS && speedMS < 0.02){
            strcpy(behave, "Stationary");
        } else if(0.02 <= speedMS && speedMS < 0.33){
            strcpy(behave, "Foraging");
        } else if(speedMS >=12){
            strcpy(behave, "Implausible");
        }
        strcpy(new_node_ptr -> behaviour, behave);
        new_node_ptr -> next = NULL;


        if(speedMS > 50) {
            return NULL;
        } else{
            return new_node_ptr;
        }
    }

}

/**
 * method that calculates time taken to do an action
 * using time from two nodes
 * and using tm struct
 * @param current
 * @param nextNode
 * @return
 */
int timeTaken(data_ptr current, data_ptr nextNode){
    struct tm timeInfo;
    int seconds1;
    int seconds2;
    int time;
    timeInfo.tm_sec = current ->sec;
    timeInfo.tm_min = current -> min;
    timeInfo.tm_hour = current -> hours;
    timeInfo.tm_mday = current -> day;
    timeInfo.tm_mon = current ->month - 1;
    timeInfo.tm_year = current -> year - 1900;
    seconds1 = mktime(&timeInfo);
 //   printf("FIRST TIME IN SEC: %d \n", seconds1);
    timeInfo.tm_sec = nextNode ->sec;
    timeInfo.tm_min = nextNode -> min;
    timeInfo.tm_hour = nextNode -> hours;
    timeInfo.tm_mday = nextNode -> day;
    timeInfo.tm_mon = nextNode ->month - 1;
    timeInfo.tm_year = nextNode -> year - 1900;
    seconds2 = mktime(&timeInfo);
    time = difftime(seconds1, seconds2);

    time = time - 3600;
    return time;
}


/**
 * writing the information in the list
 * into a text file using specified format and order
 * @param newPtr
 * @return
 */
int write_to_file(movements_ptr newPtr){
    //char const *fileName;
    int movement = 1;
    FILE *f = fopen("outputFile", "w");
    if (f == NULL){
        return -1;
    }
    while (newPtr -> next != NULL) {
        fprintf(f, "Movement: %d, %d/%d/%d, %d:%d:%d, %f, %f, %f, %f, %fm, %ds, %fm/s, %s\n", movement, newPtr->nextNode->day, newPtr->nextNode->month, newPtr -> nextNode->year,
                newPtr->nextNode->hours, newPtr->nextNode->min, newPtr -> nextNode->sec, newPtr->currentNode->latitude, newPtr->currentNode->longitude,
                newPtr -> nextNode->latitude, newPtr->nextNode->longitude, newPtr->distance, newPtr -> time, newPtr->speed, newPtr->behaviour);
        movement++;
        newPtr = newPtr -> next;
    }
    fclose(f);
    return 0;
}


//calculate times spent stationary, foraging and implausible times
void calcTotalTime(movements_ptr newPtr){
    int timeStat =0;
    int timeForag =0;
    int timeImpl =0;

    while (newPtr -> next != NULL) {
        if (strcmp(newPtr->behaviour, "Stationary") == 0) { //returns 0 if they're identical
            timeStat = timeStat + newPtr->time;
            newPtr = newPtr -> next;
        } else if (strcmp(newPtr->behaviour, "Foraging") == 0) {
            timeForag = timeForag + newPtr->time;
            newPtr = newPtr -> next;
        } else if (strcmp(newPtr->behaviour, "Implausible") == 0) {
            timeImpl = timeImpl + newPtr->time;
            newPtr = newPtr -> next;
        }
    }
    printf("Time stationary: %d\n", timeStat);
    printf("Time foraging: %d\n", timeForag);
    printf("Time implausible: %d\n", timeImpl);
}

