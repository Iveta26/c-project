//
// Created by Iveta on 10/12/2020.
//

#ifndef UNTITLED1_FUNCTIONS_H
#define UNTITLED1_FUNCTIONS_H

typedef  struct data_tag * data_ptr; //data_ptr points at data_tag

typedef struct movements * movements_ptr;

/**
 * struct to store clean data that
 * matches format specified
 */
typedef struct data_tag{
    double longitude;
    double latitude;
    int day;
    int month;
    int year;
    int hours;
    int min;
    int sec;
    data_ptr next;
}node;

/**
 * struct that stores additional data
 * to the data_tag struct and retrieves data
 * from two data_tag nodes through pointers
 */
typedef struct movements{
    data_ptr currentNode;
    data_ptr nextNode;
    double distance; //meters
    int time;//seconds
    double speed;// d/t
    char behaviour[12];
    movements_ptr next;
}move;

//data_ptr make_node(char * new_book, char * new_author, long int new_isbn, data_ptr new_node_ptr);
data_ptr make_node(double longitude, double latitude, int day, int month, int year, int hours, int min, int sec);
void insert_at_top(data_ptr * ptr_to_existing_head, data_ptr new_node_ptr);
void insert_at_top_move(movements_ptr * ptr_to_existing_head, movements_ptr new_node_ptr);
void insert_at_tail(data_ptr * ptr_to_existing_head, data_ptr new_node_ptr);
void display_list(data_ptr list_ptr);
void display_listMovement(movements_ptr list_ptr);
void free_list(data_ptr list_ptr);
double distance(data_ptr first, data_ptr second);
movements_ptr createMovementNode(data_ptr list_ptr);
int timeTaken(data_ptr current, data_ptr next);
int write_to_file(movements_ptr newPtr);
void calcTotalTime(movements_ptr newPtr);
void read_from_file();
#endif //UNTITLED1_FUNCTIONS_H
