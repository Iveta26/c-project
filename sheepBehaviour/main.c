#include <stdio.h>
#include <stdlib.h>
#include "functions.h"
int main() {

    //reading from file
    FILE *fp;// a file to read
    int dd, mm, yy, h, m, s; //variables to be read from a file
    double lat, lon;
    char buff[255];
    int stored = 0;
    int skipped = 0;
    int invalid = 0;
    data_ptr data; //to store clean data in the struct
    movements_ptr movement_ptr;
    data_ptr head = NULL;
    data_ptr headNext = NULL;
    movements_ptr headMove = NULL;
    fp = fopen("data", "r");
    if (fp == NULL) {
        fprintf(stderr, "\nError opening file\n");
        exit (1);
    }
    while (fgets(buff, 255, fp) != NULL){
        //create new node if the data is the required format
        if(sscanf( buff, "%lf, %lf, %d/%d/%d , %d:%d:%d", &lat, &lon, &dd,&mm,&yy, &h,&m,&s ) == 8){ //if all of them matches it returns 8
            //if latitude and longitude are the same just update the current node
            if(head != NULL && lat == head->latitude && lon == head ->longitude){
                skipped++;
                head ->day =dd;
                head ->month =mm;
                head ->year =yy;
                head ->hours =h;
                head ->min =m;
                head ->sec =s;
            } else{//else create a new node
                stored++;
                data = make_node(lon, lat, dd, mm, yy, h, m, s);
                insert_at_top(&head, data);
            }
        } else{
            invalid++;
        }
    }

    fclose(fp);
    printf("Stored: %d \n", stored);
    printf("Skipped: %d\n", skipped);
    printf("Invalid: %d\n", invalid);

    headNext = head;

 //creating a list that holds processed data
 //from a list of clean data that's already created
    while (headNext -> next != NULL) {
        movement_ptr = createMovementNode(headNext);
        if(movement_ptr == NULL){ //returns NULL when the speed is over 50
            //we skip that node
            headNext= headNext ->next; //move to the next one
            continue;
        }
        headNext= headNext ->next; //move to the next one
        insert_at_top_move(&headMove, movement_ptr); //and insert it at the top
    }

    //display_listMovement(headMove);
    write_to_file(headMove);
    calcTotalTime(headMove);

    return 0;
}


